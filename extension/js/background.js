let stopColor = "#FF3100",
    stopClass = "stop",
    workColor = "#97EF00",
    workClass = "work",
    breakColor = "#7107B9",
    breakClass = "break",
    busyColor = "#fbb104",
    busyClass = "busy",
    workTimeTEMP = 0,
    workTimeSegment = 0,
    breakTimeTEMP = 0,
    breakTimeSegment = 0,
    busyTimeTEMP = 0,
    busyTimeSegment = 0,
    timerStep = 1 * 1000, //in milliseconds
    timerSegment = null,
    timerInterval,
    lastStatus = 0,
    isLocked = false,
    isBlacklisted = null,
    notifs = [];

let defaultData = {
    today: new Date().toJSON().slice(0,10).split('-').reverse().join('/'),
    status: 0,
    totalDueWoBreak: 0,
    totalWork: 0,
    totalBusy: 0,
    totalNoBreak: 0,
    totalBreak: 0,
    total: 0,
    segments: [],
    timerInit: null,
    timerStopped: null,
    overtimeTotal: false,
    overtimeWork: false
};

let defaultOptions = {
    totalDue: 8*60*60,//8 * 60 * 60 8h in seconds
    toggleView : 1,
    toggleHalf : 0,
    toggleWakeup : 0,
    toggleStop : 0,
    toggleExport : 0,
    toggleLock : 0,
    toggleAlert : 0,
    toggleSound : 1,
    toggleBlacklist : 0,
    blacklistURL : [
        "https://www.facebook.com/",
        "https://www.twitter.com/",
        "https://www.instagram.com/",
    ],
};
let data = iterationCopy(defaultData);
let options = {};

chrome.browserAction.setBadgeText({text: stopClass});
chrome.browserAction.setBadgeBackgroundColor({color: stopColor});

chrome.storage.sync.get(null, function(o) {
    if(objHasData(o)){
        options = iterationCopy(o);
    }else{
        options = iterationCopy(defaultOptions);
        chrome.storage.sync.set(options);
    }
    if(options.toggleWakeup > 0 && data.status == 0 && data.timerStopped == null){
        setStatus(1, "Auto Wake Up",function(){
            sendStatus(data);
            if(options.toggleAlert > 0){
                createNotification(true,1);
            }
        });
    }
});


chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if (request.getData === true){
        sendResponse([data,options]);
    }
    if (request.resetData === true){
        sendResponse(resetData());
    }
    if (!isNaN(request.setDueTime)){
        options.totalDue = request.setDueTime;
        defaultData.totalDueWoBreak = request.setDueTime;
        sendResponse([data,options]);
        chrome.storage.sync.set(options,function(){
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                if(tabs.length){
                    chrome.tabs.sendMessage(tabs[0].id, {sendDueTimeToContent: options.totalDue});
                }
            });                    
        });
    }
    if (!isNaN(request.setStatus)){
        setStatus(request.setStatus, "Popup Action",function(){
            sendResponse(data);
            sendStatus(data,'content');
        });
    }
    if (typeof request.changeSegment === 'object' && objHasData(request.changeSegment)){
        changeSegment(request.changeSegment,function(){
            sendResponse(data);
        });
    }
    if (typeof request.updateBlacklist === 'object'){
        options.blacklistURL = request.updateBlacklist;
        sendResponse(options);
    }
    if (typeof request.setToggle === 'object' && objHasData(request.setToggle)){
        setToggle(request.setToggle,function(){
            chrome.storage.sync.set(options,function(){
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    if(tabs.length){
                        chrome.tabs.sendMessage(tabs[0].id, {sendOptionsToContent: options});
                    }
                });    
            });
        });
    }
    if (request.exportToCsv === "click"){
        exportToCsv("Stopped Manually");
    }
});
chrome.tabs.onActivated.addListener(function(inf){
    onBlackList();
    refreshTab();
});
chrome.windows.onFocusChanged.addListener(function(inf){
    onBlackList();
    refreshTab();
});
chrome.tabs.onUpdated.addListener(function(){
    onBlackList();
    refreshTab();
});
chrome.commands.onCommand.addListener(function(command){
    let execStr = 'trigger_';
    if(command.includes(execStr)){
        let option = command.replace(execStr,'');
        let bool = options[option]?0:1;
        let toggleObj = {};
        toggleObj[option] = bool;
        setToggle(toggleObj,function(){
            chrome.storage.sync.set(options,function(){
                chrome.runtime.sendMessage({sendOptionsToPopup: options});
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    if(tabs.length){
                        chrome.tabs.sendMessage(tabs[0].id, {sendOptionsToContent: options});
                    }
                });
            });
        });
    }else{
        let curStatus = data.status,
            newStatus;
        switch (command) {
            case "toggle_timer":
                newStatus = curStatus==1?2:1;
                break;
            case "stop_timer":
                newStatus = 0;
                break;
        }
        if(
            (
                newStatus == 0
                    &&
                curStatus > 0
                    &&
                window.confirm('Do you want to stop the day? \n If not, please click `Take a break`.')
            )   ||  (
                newStatus == 1
                    &&
                curStatus == 0
                    &&
                data.total > 0
                    &&
                window.confirm('You are about to reset the current timer.\n Do you wish to continue?')
            )   || (
                newStatus == 1
                    &&
                curStatus == 0
                    &&
                data.total == 0
            )   || (
                newStatus > 0
                    &&
                curStatus > 0
            )
        ){
            setStatus(newStatus, "Keyboard"+(newStatus==0?" Stop":"")+" Command",function(){
                sendStatus(data);
            });
        }
    }
});
chrome.idle.onStateChanged.addListener(function(state){
    let origin = "Auto";
    if (options.toggleLock > 0 && data.status > 0 && data.total > 0){
        let newStatus;
        if(state == 'locked'){
            if(data.status == 1) newStatus = options.toggleLock == 1?3:2;
            isLocked = true;
            origin += " Lock";
        }else{
            if(isLocked && data.status == (options.toggleLock == 1?3:2)){
                newStatus = 1;
                isLocked = false;
                origin += " Unlock";
            }
        }
        if(newStatus > 0){
            setStatus(newStatus, origin,function(){
                sendStatus(data);
            });
        }
    }
});
function setToggle(toggleObj,callback){
    let toggle = Object.keys(toggleObj)[0],
        bool = Number(toggleObj[toggle]);
    options[toggle] = bool;
    if(typeof callback == 'function'){
        callback();
    }
}
function refreshTab(){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        if(tabs.length){
            chrome.tabs.sendMessage(tabs[0].id, {refreshContent: data, sendOptionsToContent: options});
        }
    });
};
function onBlackList(){
    if(options.toggleBlacklist > 0 && objHasData(options.blacklistURL) && data.status > 0){
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            if(tabs.length){
                let url = tabs[0].url;
                for (let ban of options.blacklistURL) {
                    let ban_no_protocol = removeHttp(ban);
                    if(url.includes(ban_no_protocol)){
                        if(data.status != 2){
                            isBlacklisted = ban_no_protocol;
                            setStatus(2, "Auto Break (Blacklist)",function(){
                                sendStatus(data);
                            });
                        }else{
                            isBlacklisted = ban_no_protocol;
                        }
                        break;
                    }
                };
                if(isBlacklisted != null && !url.includes(isBlacklisted) && data.status == 2){
                    isBlacklisted = null;
                    setStatus(1, "Auto Work (Blacklist)", function(){
                        sendStatus(data);
                    });
                }
            }
        });
    }
};
function createNotification(autoNotif = false, newStatus){
    let notifName = 'notif';
    let notifOptions = {
        iconUrl: "images/WPB_icon128.png",
    };
    if(autoNotif === true){
        if(newStatus == 1){
            notifName += 'Autostart';
            notifOptions['title'] = "WPB - 💻 Time to work";
            notifOptions['message'] = "Be productive!";
        }else if(newStatus == 2){
            notifName += 'Break';
            notifOptions['title'] = "WPB - ☕️ Time for a break";
            notifOptions['message'] = "Enjoy your break..";
        }else if(newStatus == 3){
            notifName += 'Busy';
            notifOptions['title'] = "WPB - 💼 Busy, do not disturb";
            notifOptions['message'] = "Work but not on computer.";
        }
        notifOptions['type'] = "basic";
    }else{
        if(data.overtimeWork){
            let diffTime = data.totalNoBreak - options.totalDue,
                overtimeLimit = options.totalDue*1/16;

            if(diffTime < overtimeLimit){
                notifName += 'Duetime';
                notifOptions['type'] = "progress";
                notifOptions['progress'] = 100;
                notifOptions['title'] = "WPB - Due Time Reached!";
                notifOptions['message'] = "Well done, your working hours have been accomplished.";
            }else{
                notifName += 'Overtime';
                notifOptions['type'] = "basic";
                notifOptions['title'] = "WPB - " + diffTime.sToTime() + " more!";
                notifOptions['message'] = "You have already worked " + diffTime.sToTime() + " over the due time.";
            }
        }else{
            let diffTime = options.totalDue - data.totalNoBreak,
                diffProgress = data.totalNoBreak / options.totalDue * 100,
                dateVal =  new Date(data.timerInit),
                eta;

            dateVal.setSeconds( dateVal.getSeconds() + data.totalDueWoBreak);
            eta = dateVal.toLocaleTimeString();

            notifName += 'Soon';
            notifOptions['type'] = "progress";
            notifOptions['progress'] = Math.round(diffProgress);
            notifOptions['title'] = "WPB - " + diffTime.sToTime() + " left";
            notifOptions['message'] = "ETEW : " + eta;
        }
    }

    if (!notifs.includes(notifName) ||  autoNotif === true){
        if (notifs.includes(notifName) && autoNotif === true){
            chrome.notifications.clear(notifName);
        }
        chrome.notifications.create(notifName,notifOptions,function(id){
            notifs.push(id);
            if (options.toggleSound > 0){
                var notifSound = new Audio("audio/" + id + ".mp3");
                notifSound.play();
                notifSound.volume = 0.3;
            }
            setTimeout(function(){
                if (options.toggleSound > 0){
                    notifSound.pause();
                }
                chrome.notifications.clear(id);
            },5000);
        });
    }
};
function setStatus(status,origin,callback){
    lastStatus = data.status;
    if(status == 0){
        let timerStopped = new Date();
        data.timerStopped = timerStopped;
    }else{
        if(lastStatus == 0){
            resetData();
        }
    }
    data.status = status;
    if(typeof callback == 'function'){
        callback();
    }
    
    let statusText,
        statusColor;
    
    if (data.timerInit === null){
        data.timerInit = new Date();
    }
    switch (status) {
        case 0:
            statusText = stopClass;
            statusColor = stopColor;
            break;
        case 1:
            statusText = workClass;
            statusColor = workColor;
            break;
        case 2:
            statusText = breakClass;
            statusColor = breakColor;
            break;
        case 3:
            statusText = busyClass;
            statusColor = busyColor;
            break;
    }
    chrome.browserAction.setBadgeText({text: statusText});
    chrome.browserAction.setBadgeBackgroundColor({color: statusColor});

    if(options.toggleAlert > 0 && status > 0){
        createNotification(true, status);
    }

    if(timerInterval) clearInterval(timerInterval);

    let nowLast = new Date();
    if(lastStatus==1){
        workTimeSegment = diffSeconds(nowLast, timerSegment);
        data.totalWork = workTimeTEMP + workTimeSegment;
        workTimeTEMP = data.totalWork;
        data.totalNoBreak = data.totalWork + data.totalBusy;
        data.total = data.totalNoBreak + data.totalBreak;
    }else if(lastStatus == 2){
        breakTimeSegment = diffSeconds(nowLast, timerSegment);
        data.totalBreak = breakTimeTEMP + breakTimeSegment;
        data.totalDueWoBreak = options.totalDue + data.totalBreak;
        breakTimeTEMP = data.totalBreak;
        data.total = data.totalNoBreak + data.totalBreak;
    }else if(lastStatus == 3){
        busyTimeSegment = diffSeconds(nowLast, timerSegment);
        data.totalBusy = busyTimeTEMP + busyTimeSegment;
        busyTimeTEMP = data.totalBusy;
        data.totalNoBreak = data.totalWork + data.totalBusy;
        data.total = data.totalNoBreak + data.totalBreak;
    }
    if(status > 0){
        if(lastStatus > 0){
            updateSegments(lastStatus,origin);
        }
        timerSegment = new Date();
        timerInterval = setInterval(function(){
            let now = new Date();
            
            if(status==1){
                workTimeSegment = diffSeconds(now, timerSegment);
                data.totalWork = workTimeTEMP + workTimeSegment;
                data.totalNoBreak = data.totalWork + data.totalBusy;
            }else if(status==2){
                breakTimeSegment = diffSeconds(now, timerSegment);
                data.totalBreak = breakTimeTEMP + breakTimeSegment;
                data.totalDueWoBreak = options.totalDue + data.totalBreak;
            }else if(status==3){
                busyTimeSegment = diffSeconds(now, timerSegment);
                data.totalBusy = busyTimeTEMP + busyTimeSegment;
                data.totalNoBreak = data.totalWork + data.totalBusy;
            }
            data.total = data.totalNoBreak + data.totalBreak;
            data.overtimeWork = data.totalNoBreak > options.totalDue;
            data.overtimeTotal = data.total > options.totalDue;
            updateSegments(status,origin);
            soonLimit = options.totalDue*1/16;
            if(options.toggleAlert > 0 && data.totalNoBreak >= options.totalDue - soonLimit){
                createNotification(status);
            }
            if(options.toggleStop > 0 && data.totalNoBreak >= options.totalDue){
                setStatus(0, "Auto Stop");
            }            
        }, timerStep);
    }else{
        updateSegments(status,origin);
        if(options.toggleExport > 0){
            exportToCsv(origin);
        }            
    }
};
function changeSegment(changedSegment,callback){
    data.total = data.totalWork = data.totalBreak = data.totalBusy = data.totalNoBreak = data.totalDueWoBreak = 0;
    data.overtimeTotal = data.overtimeWork = false;
    for (let s of data.segments) {
        if(s.id == changedSegment.id){
            s.status = changedSegment.status;
        }
        switch (s.status) {
            case 1:
                data.totalWork += s.duration;
                break;
            case 2:
                data.totalBreak += s.duration;
                break;
            case 3:
                data.totalBusy += s.duration;
                break;
        }
    };
    data.totalNoBreak = data.totalWork + data.totalBusy;
    data.totalDueWoBreak = options.totalDue + data.totalBreak;
    data.total = data.totalNoBreak + data.totalBreak;
    data.overtimeWork = data.totalNoBreak > options.totalDue;
    data.overtimeTotal = data.total > options.totalDue;

    if(typeof callback == 'function'){
        callback();
    }
    refreshTab();
};
function updateSegments(status,origin){
    let prevSegStatus,
    prevSegment = {},
    segLength = data.segments.length;
    if (segLength){
        prevSegment = data.segments[segLength - 1];
        prevSegStatus = prevSegment.status;
    }
    if(prevSegStatus == status){
        switch (status) {
            case 1:
                prevSegment.duration = workTimeSegment;
                break;
            case 2:
                prevSegment.duration = breakTimeSegment;
                break;
            case 3:
                prevSegment.duration = busyTimeSegment;
                break;
        }
    }else{
        if(segLength){
            prevSegment["ended"] = prevSegment.started + (prevSegment.duration*1000); //duration is in seconds... JS time in millisecond
        }
        let duration;
        switch (status) {
            case 0:
                duration = 0;
                break;
            case 1:
                duration = workTimeSegment;
                break;
            case 2:
                duration = breakTimeSegment;
                break;
            case 3:
                duration = busyTimeSegment;
                break;
        }
        data.segments.push({
            id: segLength,
            status: status,
            origin: origin
        });
        if(status > 0){
            data.segments[data.segments.length - 1]["started"] = new Date().getTime();
            data.segments[data.segments.length - 1]["duration"] = duration;
        }
    }
    sendData(data);
};

function sendData(d){
    chrome.runtime.sendMessage({sendDataToPopup: d});
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        if(tabs.length){
            chrome.tabs.sendMessage(tabs[0].id, {sendDataToContent: d});
        }
    });
};
function sendStatus(d, target='both'){
    if(target == 'both' || target == 'popup'){
        chrome.runtime.sendMessage({sendDataToPopup: data});
    }
    if(target == 'both' || target == 'content'){
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            if(tabs.length){
                chrome.tabs.sendMessage(tabs[0].id, {sendStatusToContent: data.status});
            }
        });
    }
};
function sendData(d){
    chrome.runtime.sendMessage({sendDataToPopup: d});
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        if(tabs.length){
            chrome.tabs.sendMessage(tabs[0].id, {sendDataToContent: d});
        }
    });
};

function resetData(){
    timerSegment = null;
    notifs = [];
    workTimeSegment = workTimeTEMP = breakTimeSegment = breakTimeTEMP = busyTimeSegment = busyTimeTEMP = 0;
    data = iterationCopy(defaultData);
    data.totalDueWoBreak = options.totalDue;
    return data;
};

function diffSeconds(dt2, dt1){
    var diff =(dt2.getTime() - dt1.getTime()) / 1000;
    return Math.abs(Math.round(diff)); 
};
Number.prototype.sToTime = function () {
    var sec_num = parseInt(this, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};
function iterationCopy(src) {
    let target = {};
    for (let prop in src) {
        if (src.hasOwnProperty(prop)) {
            if(Array.isArray(src[prop])){
                target[prop] = [...src[prop]];
            }else{
                target[prop] = src[prop];
            }
        }
    }
    return target;
};
function objHasData(o){
    return Object.keys(o).length > 0;
};
function removeHttp(url) {
    let disallowed = new Array('https://www.','https://','http://www.','http://');
    let newUrl = url;
    for (let d of disallowed) {
        if(url.indexOf(d) > -1) {
            newUrl = url.replace(d, '');
            break;
        }
    };
    return newUrl;
};
function getStatusString(s){
    let strGrp = {};
    switch (s) {
        case 1:
            title = "Worked";
            classStatus = workClass;
            break;
        case 2:
            title = "Break";
            classStatus = breakClass;
            break;
        case 3:
            title = "Busy";
            classStatus = busyClass;
            break;
    }
    strGrp["title"] = title;
    strGrp["class"] = classStatus;
    return strGrp;
};
function exportToCsv(origin) {
    let filename = "workTimeLine_" + data.today.replace(/\//g,".") + ".csv",
        separator = [" "," "];
    let dataToExport = new Array(
        ["Label","Recorded data"],
        ["Date", data.today],
        ["Due Time",options.totalDue.sToTime()],
        (data.overtimeWork?["!!OVERTIME!!",(data.totalNoBreak-options.totalDue).sToTime()]:[]),
        ["Total Worked", data.totalWork.sToTime()],
        ["Total Busy", data.totalBusy.sToTime()],
        ["Total Break", data.totalBreak.sToTime()],
        ["Total",data.total.sToTime()],
        ["Time Started at", new Date(data.timerInit).toLocaleTimeString()],
        ["Time Stopped at", new Date(data.timerStopped).toLocaleTimeString()],
        separator
    );
    for (let segment of data.segments) {
        let rowHead = [],
            row1 = [],
            row2 = [],
            row3 = [];
        if(segment.status > 0){
            dataToExport = [...dataToExport,separator];
                
            let strGrp = getStatusString(segment.status);
            let rowTitle = strGrp.title + " - Segment " + (segment.id + 1);
            rowHead = [rowTitle,segment.origin];
            row1 = ["Duration",segment.duration.sToTime()];
            row2 = ["Started at",new Date(segment.started).toLocaleTimeString()];
            row3 = ["Stopped at",new Date(segment.ended).toLocaleTimeString()]
        }else{
                let rowAutoStop = ["",origin];
                dataToExport = [...dataToExport,rowAutoStop];
        }
        dataToExport = [...dataToExport,rowHead,row1,row2,row3];
    };
    let processRow = function (row) {
        let finalVal = '';
        for (let j = 0; j < row.length; j++) {
            let innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            };
            let result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ',';
            finalVal += result;
        }
        return finalVal + '\n';
    };

    let csvFile = '';
    for (let i = 0; i < dataToExport.length; i++) {
        csvFile += processRow(dataToExport[i]);
    }

    let blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        let link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            let url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
};
