let $workPB =   $("<div />",{
                    id: 'workPB'
                }),
    $overTimeLayer =   $("<div />",{
                    id: 'workPB-overtime'
                }),
    docWidth,
    pBSegment = 'workPB-segment';
let workClass = "work",
    breakClass = "break",
    busyClass = "busy",
    stopClass = "stop";
let countSegment = 0,
    lastStatus = 0,
    data = {},
    options = {};

$(document).ready(function(){
    $('body').append($workPB);
    docWidth = $($workPB).innerWidth();
    chrome.runtime.sendMessage({getData: true}, function(t) {
        let d = t[0],
        o = t[1];
        if (objHasData(d)){
            if (objHasData(o)){
                options = o;
            }
            data = d;
            $($workPB).attr("data-tooltip",data.totalNoBreak.sToTime());
            $($workPB).append(
                $overTimeLayer.attr("data-tooltip",options.totalDue.sToTime()).hide()
            );
        }
    });
    $(window).resize(function(){
        docWidth = $($workPB).innerWidth();
    })
});
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if (typeof request.sendDueTimeToContent !== 'undefined' && !isNaN(request.sendDueTimeToContent)){
        options.totalDue = request.sendDueTimeToContent;
        countSegment = 0;
        $($overTimeLayer).width(docWidth).css("flex","0 0 "+docWidth).attr("data-tooltip",options.totalDue.sToTime()).hide();
        reInitPB();
    }
    if (typeof request.sendOptionsToContent === 'object' && objHasData(request.sendOptionsToContent)){
        if(objHasData(request.sendOptionsToContent)){
            options = request.sendOptionsToContent;
            for(option in options){
                let bool = options[option];
                if(typeof window[option] == 'function'){
                    window[option](bool);
                }
            }
        }
    }
    if (typeof request.sendStatusToContent !== 'undefined' && !isNaN(request.sendStatusToContent)){
        lastStatus = data.status;
        data.status = request.sendStatusToContent;
        if(lastStatus > 0){
            if(data.overtimeTotal || data.status == 2){
                updateAllSegments();
            }else{
                updateSegment(data.segments.length - 1);
            }
        }
        if(lastStatus == 0 && data.status == 1){
            countSegment = 0;
            reInitPB();
        }
        createSegment(data.status);
    }
    if (typeof request.sendDataToContent === 'object' && objHasData(request.sendDataToContent)){
        if(objHasData(request.sendDataToContent)){
            data = request.sendDataToContent;
            if(data.overtimeTotal || data.status == 2){
                updateAllSegments();
            }else{
                updateSegment(data.segments.length - 1);
            }
        }
    }
    if (typeof request.refreshContent === 'object' && objHasData(request.refreshContent)){
        if(objHasData(request.refreshContent)){
            data = request.refreshContent;
            refreshAllSegments();
        }
    }
});
function toggleView(val){
    if(val) $($workPB).show();
    else $($workPB).hide();
};
function toggleHalf(val){
    let defaultHeight = 8,
        halfHeight = defaultHeight/2,
        height;
    
    if(val) height = halfHeight;
    else height = defaultHeight;
    document.documentElement.style.setProperty('--wpb-height', height+'px');
};
function createSegment(status){
    let $segment = $("<div />",{
            class: pBSegment
        }),
        classStatus,
        segmentID,
        attributes = {};
    switch (status) {
        case 1:
            classStatus = workClass;
            break;
        case 2:
            classStatus = breakClass;
            break;
        case 3:
            classStatus = busyClass;
            break;
        case 0:
            classStatus = stopClass;
            break;
    }
    segmentID = countSegment;
    attributes = {
        id: pBSegment+segmentID
    }
    $($workPB).append($segment.attr(attributes).addClass(classStatus));
    countSegment++;
};
function reInitPB(){
    $($workPB).children('.' + pBSegment).remove();
    $overTimeLayer.hide();
};
function refreshAllSegments(){
    reInitPB();
    countSegment = 0;
    $.each(data.segments,function(i,segment){
        createSegment(segment.status);
        updateSegment(i);
    });
};
function updateAllSegments(){
    $.each(data.segments,function(i,segment){
        updateSegment(i);
    });
};
function updateSegment(sI){
    let curSegment = data.segments[sI],
        curTime = curSegment.duration,
        curSegmentID = curSegment.id,
        maxTime = data.overtimeWork?(data.total+(8/docWidth*data.total)):(data.totalDueWoBreak+(8/docWidth*data.totalDueWoBreak)), 
        widthSegment = Math.round(curTime/maxTime*1000)/10 + '%',
        flexSegment = "0 0 "+widthSegment;

    if(data.overtimeWork){
        let overTimeWidth = docWidth - (docWidth * options.totalDue / data.totalNoBreak);
        $overTimeLayer.width(overTimeWidth).css("flex","0 0 "+overTimeWidth).show();
        $($workPB).attr('data-tooltip',data.totalNoBreak.sToTime()+' on '+options.totalDue.sToTime());
    }else{
        $overTimeLayer.width(docWidth).css("flex","0 0 "+docWidth).hide();
        $($workPB).attr('data-tooltip',data.totalNoBreak.sToTime());
    }
    if(curTime > 0){
        $('#' + pBSegment + curSegmentID).width(widthSegment).css("flex",flexSegment).attr('data-tooltip',curTime.sToTime());
    }else{
        if(data.overtimeWork){
            $('#' + pBSegment + curSegmentID).addClass('fixedToRight');
        }
    }
};
Number.prototype.sToTime = function () {
    var sec_num = parseInt(this, 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};
function sToHours(duration){
    return duration / 60 / 60;
};
function objHasData(o){
    return Object.keys(o).length > 0;
};
