let workClass = "work",
    breakClass = "break",
    busyClass = "busy",
    stopClass = "stop",
    startLabelFull = "Start Working",
    startLabelShort = "Start";
let btnStatusStart = document.getElementById('btnStatusStart'),
    btnStatusStop = document.getElementById('btnStatusStop'),
    btnStatusBreak = document.getElementById('btnStatusBreak'),
    btnStatusBusy = document.getElementById('btnStatusBusy'),
    btnResetData = document.getElementById('btnResetData'),
    resetBackdrop = document.getElementById('resetBackdrop'),
    btnOKDueTime = document.getElementById('btnOKDueTime'),
    btnExportData = document.getElementById('btnExportData'),
    inputOKDueTime = document.getElementById('inputOKDueTime'),
    formOKDueTime = document.getElementById('formOKDueTime'),
    formBlacklistURL = document.getElementById('formBlacklistURL'),
    tBlacklistDetails = document.getElementById('tBlacklistDetails'),
    lblTodayDate = document.getElementById('todayDate'),
    lblStartedTime = document.getElementsByClassName('startedTime'),
    lblStoppedTime = document.getElementsByClassName('stoppedTime'),
    lblETA = document.getElementsByClassName('etaTime'),
    lblETARefreshed = document.getElementsByClassName('etaTimeRefreshed'),
    lblTotalTime = document.getElementById('totalTime'),
    lblWorkTime = document.getElementById('workTime'),
    lblBusyTime = document.getElementById('busyTime'),
    lblBreakTime = document.getElementById('breakTime'),
    lblDueTime = document.getElementsByClassName('dueTime'),
    lblDueTimeError = document.getElementById('dueTimeError'),
    divDetailSession = document.querySelector('.tab-details > .session'),
    toggleButton = document.getElementsByClassName("toggle-button"),
    toggleSound = document.getElementById("tSound");

let data = {},
    options = {};

btnStatusBusy.addEventListener('click', function(){
    updateStatus(3);
});
btnStatusBreak.addEventListener('click', function(){
    updateStatus(2);
});
btnStatusStart.addEventListener('click', function(){
    updateStatus(1);
});
btnStatusStop.addEventListener('click', function(){
    updateStatus(0);
});
btnResetData.addEventListener('click', resetData);
btnExportData.addEventListener('click', function(){
    if(objHasData(data) && data.segments.length){
        if(data.timerStopped !== null){
            chrome.runtime.sendMessage({exportToCsv: "click"});
        }else{
            alert("Please stop the timer to export.");
        }
    }else{
        alert("Nothing to export.");
    }
});
formOKDueTime.addEventListener('submit', updateDueTime);
formBlacklistURL.addEventListener('submit', function(e){
    e.preventDefault();
    let item = this.querySelector("#tBlacklistInput").value;
    let list = this.querySelector(".url-list");
    options.blacklistURL.push(item);
    chrome.runtime.sendMessage({updateBlacklist: options.blacklistURL}, function(o) {
        updateOptions(o);
    });
});
toggleSound.addEventListener('click', function() {
    let oToggle = {},
        val = this.checked;
    oToggle.toggleSound = Number(val);
    options.toggleSound = Number(val);
    chrome.runtime.sendMessage({setToggle: oToggle});
});
[...toggleButton].forEach(group => {
    let radios = group.getElementsByTagName("input"),
        toggleName = radios[0].getAttribute("name");
    [...radios].forEach(radio => {
        radio.addEventListener('click', function() {
            let oToggle = {},
                val = radio.value,
                labels = this.parentNode.parentNode.querySelector('.label');
            oToggle[toggleName] = val;
            options[toggleName] = val;
            chrome.runtime.sendMessage({setToggle: oToggle});
            
            let l_show = labels.querySelector('.l'+val),
                l_hidden = labels.querySelectorAll(':not(.l'+val+')');
            l_show.classList.remove("hidden");
            for (var i = 0; i < l_hidden.length; ++i) {
                l_hidden[i].classList.add('hidden');
            }
                
            if(toggleName == "toggleBlacklist"){
                if(val > 0){
                    tBlacklistDetails.classList.remove("hidden");
                    tBlacklistDetails.previousElementSibling.classList.remove("hidden");
                }else{
                    tBlacklistDetails.classList.add("hidden");
                    tBlacklistDetails.previousElementSibling.classList.add("hidden");
                }
            }
        });
    });
});

chrome.runtime.sendMessage({getData: true}, function(t) {
    let d = t[0],
        o = t[1];
    updateOptions(o);
    updatePopup(d);
});
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if (typeof request.sendDataToPopup === 'object' && objHasData(request.sendDataToPopup)){
        updatePopup(request.sendDataToPopup);
    }
    if (typeof request.sendOptionsToPopup === 'object' && objHasData(request.sendOptionsToPopup)){
        updateOptions(request.sendOptionsToPopup);
    }
});
Array.prototype.del = function(value) {
    return this.splice( this.indexOf(value), 1 );
};
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1)
}
Number.prototype.sToTime = function () {
    let sec_num = parseInt(this, 10);
    let hours   = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};
function sToHours(duration){
    return duration / 60 / 60;
};
function objHasData(o){
    return Object.keys(o).length > 0;
};
function getStatusString(s){
    let strGrp = {};
    switch (s) {
        case 1:
            title = "Worked";
            classStatus = workClass;
            break;
        case 2:
            title = "Break";
            classStatus = breakClass;
            break;
        case 3:
            title = "Busy";
            classStatus = busyClass;
            break;
    }
    strGrp["title"] = title;
    strGrp["class"] = classStatus;
    return strGrp;
}
function updateStatus(newStatus) {
    let status = data.status;
    if(
        (newStatus == 0 && status > 0 && window.confirm('Do you want to stop the day? \n If not, please click `Take a break`.'))
        || (newStatus > 0)
    ){
        chrome.runtime.sendMessage({setStatus: newStatus}, function(d) {
            updatePopup(d);
        });
    }
};
function dueTimeError(msg){
    lblDueTimeError.innerText = msg;
    if(lblDueTimeError.classList.contains('fadeOut')){
        lblDueTimeError.classList.remove('fadeOut');
    }
    setTimeout(function(){lblDueTimeError.classList.add('fadeOut');}, 1);
};
function updateDueTime() {
    let newDueTime = inputOKDueTime.value;
    if (!isNaN(Number(newDueTime)) && Number(newDueTime) > 0){
        dueTime = Number(newDueTime) * 60 * 60;
        options.totalDue = dueTime;
        chrome.runtime.sendMessage({setDueTime: dueTime}, function(t) {
            let d = t[0],
                o = t[1];
            updateOptions(o);
            updatePopup(d);
        });
    }else{
        dueTimeError('The due time must be a number over 0.');
    }
};
function addToList(item, list, obj) {
    if (item != ''){
        let newItem = document.createElement("li"),
            newItemSpan = document.createElement("span"),
            newItemText = document.createTextNode(item),
            newItemDel = document.createElement("i");
        newItemDel.classList.add("url-del");
        newItemSpan.append(newItemText);
        newItem.append(newItemDel,newItemSpan);
        list.append(newItem);

        newItemDel.addEventListener('click', function(e){
            e.preventDefault();
            let line = this.parentNode;
            line.parentNode.removeChild(line);
            if(typeof obj === 'object'){
                obj.del(item);
                chrome.runtime.sendMessage({updateBlacklist: obj}, function(o) {
                    updateOptions(o);
                });
            }    
        });
    }
};
function resetData() {
    if(window.confirm('You are about to reset the current timer.\n Do you wish to continue?')){
        chrome.runtime.sendMessage({resetData: true}, function(d) {
            updatePopup(d);
        });
    }
};
function updateTime(d){
    let startStopTimes = function(grpEl,val,adder = 0){
        [...grpEl].forEach((el) => {
            if (val === null){
                el.innerHTML = "&mdash;";
            }else{
                let dateVal =  new Date(val);
                dateVal.setSeconds( dateVal.getSeconds() + adder);
                el.innerText = dateVal.toLocaleTimeString();
            }
        });
    };
    lblTodayDate.innerText = d.today;
    let lblOvertime = document.getElementById("overtime"),
        overtimeDiff = d.totalNoBreak - options.totalDue;
    if(d.overtimeWork){
        if(lblOvertime == null){
            let overtimeGroup = document.createElement("p"),
                overtimeLabel = document.createElement("label"),
                overtimeValue = document.createElement("span"),
                overtimeLText = document.createTextNode("Overtime :"),
                overtimeVText = document.createTextNode(overtimeDiff.sToTime()),
                parent = lblWorkTime.parentNode.parentNode;
            
            overtimeGroup.classList.add("danger");
            overtimeLabel.appendChild(overtimeLText);
            overtimeValue.setAttribute("id","overtime");
            overtimeValue.appendChild(overtimeVText);
            overtimeGroup.append(overtimeLabel,overtimeValue);
            parent.insertBefore(overtimeGroup, lblWorkTime.parentNode);
        }else{
            lblOvertime.innerText = overtimeDiff.sToTime();
        }
    }else{
        if(lblOvertime != null){
            lblOvertime.parentNode.parentNode.removeChild(lblOvertime.parentNode);
        }
    }
    lblWorkTime.innerText = d.totalWork.sToTime();
    lblBusyTime.innerText = d.totalBusy.sToTime();
    lblBreakTime.innerText = d.totalBreak.sToTime();
    lblTotalTime.innerText = d.total.sToTime();
    [...lblDueTime].forEach((el) => {
        el.innerText = options.totalDue.sToTime();
    });
    inputOKDueTime.value = Math.round(sToHours(options.totalDue)*1000)/1000;
    startStopTimes(lblStartedTime, d.timerInit);
    startStopTimes(lblStoppedTime, d.timerStopped);
    startStopTimes(lblETA, d.timerInit, options.totalDue);
    startStopTimes(lblETARefreshed, d.timerInit, d.totalDueWoBreak);
    if (d.segments.length){
        for (let segment of data.segments) {
                if(segment.status > 0){
                let getDOM = document.getElementById("segment"+(segment.id+1));
                if(typeof(getDOM) == 'undefined' || getDOM == null){
                    createNewSegment(segment);
                }else{
                    updateSegmentLines(segment);
                }
            }
        };
    }else{
        [].forEach.call(document.querySelectorAll('.session .segment'),function(e){
            e.parentNode.removeChild(e);
        });
    }
}
function createNewSegment(segment){
    let newSegmentDOM = document.createElement("div"),
        newSegmentTitle = document.createElement("h2"),
        newSegmentBtStatus = document.createElement("button"),
        newSegmentToggler = document.createElement("input"),
        newSegmentTogLabel = document.createElement("label"),
        newSegmentContent = document.createElement("div");

    newSegmentDOM.classList.add('segment');
    newSegmentDOM.setAttribute("data-id", segment.id);
    newSegmentDOM.setAttribute("data-status", segment.status);
    newSegmentContent.classList.add('content');
    newSegmentToggler.setAttribute("type","checkbox");
    newSegmentToggler.setAttribute("name","segments");

    let strGrp  = getStatusString(segment.status);

    let id = "segment"+ (segment.id + 1);

    let title = strGrp.title + " - Segment " + (segment.id + 1);
    let titleDOM = document.createTextNode(title);

    newSegmentBtStatus.classList.add("badge");
    newSegmentToggler.setAttribute("id",id);
    newSegmentTogLabel.setAttribute("for",id);
    newSegmentTitle.classList.add(strGrp.class);
    newSegmentTitle.append(newSegmentBtStatus,titleDOM);

    updateSegmentLines(segment, newSegmentContent);

    newSegmentDOM.append(newSegmentTitle,newSegmentToggler,newSegmentTogLabel,newSegmentContent);
    divDetailSession.append(newSegmentDOM);
    newSegmentBtStatus.addEventListener('click', function(){
        let last_segment = data.segments[data.segments.length - 1];
        if(segment.id != last_segment.id || (segment.id == last_segment.id && data.timerStopped !== null)){
            let segmentNode = this.parentNode.parentNode,
                h2 = this.parentNode,
                segID = Number(segmentNode.getAttribute("data-id")),
                segStatus = Number(segmentNode.getAttribute("data-status")),
                newStatus = segStatus>2?1:segStatus+1;
                segment = {
                    id: segID,
                    status: newStatus
                };
            chrome.runtime.sendMessage({changeSegment: segment}, function(d) {
                let strGrp = getStatusString(newStatus);
                h2.classList = '';
                h2.classList.add(strGrp.class);
                segmentNode.setAttribute("data-status",newStatus);
                let title = strGrp.title + " - Segment " + (segID + 1);
                let titleDOM = document.createTextNode(title);
                h2.innerHTML = '';
                h2.append(newSegmentBtStatus,titleDOM);
                updatePopup(d);
            });
        }else{
            alert("Please stop the timer before modifying the last segment.");
        }
    });
}
function updateSegmentLines(segment,container){
    if (container != undefined){
        let sOrigin = segment.origin,
            newSegmentOrigin = document.createElement("p"),
            originValue = document.createTextNode(sOrigin);
        newSegmentOrigin.classList.add("segment-origin");
        newSegmentOrigin.appendChild(originValue);
        container.append(newSegmentOrigin);
    }
    for (let action of new Array("started","ended","duration")) {
        let labelText = action.capitalize() + (action=="duration"?" :":" at :"),
            labelLine = document.createTextNode(labelText),
            actionExists = segment[action]!==undefined,
            sValue = actionExists?segment[action]:"\u2014",
            sValueFormated,
            id = "segment"+ (segment.id + 1);

        switch (action) {
            case "ended":
            case "started":
                sValueFormated = actionExists?new Date(sValue).toLocaleTimeString():sValue;
                break;
            case "duration":
                sValueFormated = actionExists?sValue.sToTime():sValue;
                break;
        }

        let valueLine = document.createTextNode(sValueFormated);

        let getLineID = document.getElementById(id+action);
        if(typeof(getLineID) == 'undefined' || getLineID == null){
            let newSegmentLine = document.createElement("p"),
                newSegmentLineLabel = document.createElement("label"),
                newSegmentLineValue = document.createElement("span");
                
            newSegmentLineLabel.appendChild(labelLine);
            newSegmentLineValue.setAttribute("id",id+action);
            newSegmentLineValue.appendChild(valueLine);
            newSegmentLine.append(newSegmentLineLabel,newSegmentLineValue);
            container.append(newSegmentLine);
        }else{
            getLineID.replaceChild(valueLine,getLineID.childNodes[0]);
        }
    };
};
function updateOptions(o){
    if (objHasData(o)){
        options = o;
        toggleSound.checked = Boolean(options.toggleSound);
        [...toggleButton].forEach(group => {
            let radios = group.getElementsByTagName("input"),
                toggleName = radios[0].getAttribute("name"),
                labels = group.parentNode.querySelector('.label');
            
            let val = options[toggleName];
            
            radios[Number(val)].checked = true;

                        
            let l_show = labels.querySelector('.l'+val),
                l_hidden = labels.querySelectorAll(':not(.l'+val+')');
            l_show.classList.remove("hidden");
            for (var i = 0; i < l_hidden.length; ++i) {
                l_hidden[i].classList.add('hidden');
            }

            if(toggleName == "toggleBlacklist"){
                if(val > 0){
                    tBlacklistDetails.classList.remove("hidden");
                    tBlacklistDetails.previousElementSibling.classList.remove("hidden");
                }else{
                    tBlacklistDetails.classList.add("hidden");
                    tBlacklistDetails.previousElementSibling.classList.add("hidden");
                }
                if(options.blacklistURL){
                    let list = tBlacklistDetails.querySelector(".url-list");
                    list.innerHTML = '';
                    for (let item of options.blacklistURL) {
                        addToList(item,list,options.blacklistURL);
                    };
                }
            }
        });
    }
};
function updatePopup(d,callback){
    if (objHasData(d)){
        data = d;
        let status = data.status;

        updateTime(data);

        if(typeof callback == 'function'){
            callback();
        }
    
        btnStatusStart.classList = '';
        switch (status) {
            case 0:
                btnOKDueTime.disabled = false;
                inputOKDueTime.disabled = false;
                btnStatusStop.disabled = true;
                btnStatusStart.disabled = false;
                btnStatusStart.innerText = startLabelFull;
                btnStatusBreak.disabled = true;
                btnStatusBusy.disabled = true;
                break;
            case 1:
                btnOKDueTime.disabled = true;
                inputOKDueTime.disabled = true;
                btnStatusStop.disabled = false;
                btnStatusStart.disabled = true;
                btnStatusBreak.disabled = false;
                btnStatusBusy.disabled = false;
                break;
            case 2:
                btnOKDueTime.disabled = true;
                inputOKDueTime.disabled = true;
                btnStatusStop.disabled = false;
                btnStatusStart.classList.add(breakClass);
                btnStatusStart.innerText = startLabelShort;
                btnStatusStart.disabled = false;
                btnStatusBreak.disabled = true;
                btnStatusBusy.disabled = false;
                break;
            case 3:
                btnOKDueTime.disabled = true;
                inputOKDueTime.disabled = true;
                btnStatusStop.disabled = false;
                btnStatusStart.classList.add(busyClass);
                btnStatusStart.innerText = startLabelShort;
                btnStatusStart.disabled = false;
                btnStatusBreak.disabled = false;
                btnStatusBusy.disabled = true;
                break;
        }
        resetBackdrop.classList = '';
        if(data.total == 0 || status > 0){
            resetBackdrop.classList.add("hidden");
        }
    }else{
        console.warn("No data retrieved in Popup", data);
    }
};