# Work progressbar extension
## Upgrades TODO
### Done for 1.6
√ Fix auto wakeup. Is triggered for no reason. [needs more testing but √]
√ Add toggle Automatic export on stop + functionality
√ Add auto stop on duetime toggle + function


### 2.0 (to come)
**Segments can be named in order to track each task. Therefore need to be able to create new segments from the current status**
- Add the generate new segment functionality (from current segment status)
- Add the change segment label functionality
**Statistics should give insight on the user activities**
- Add statistics menu [STATISTICS] (50% width,under current menu)
- Add a chart pie (using css for most part) and some other views
- Keep data in the most simple and fastforward way (json?) in storage.sync
- Check the amount of data available on Chrome Sync.... (to avoid overloading)
- Since data is synchronized and saved, we could add another menu :
- Add menu history [HISTORY] (50% width,under current menu)



## Publish TODO
- Add more information about the timer itself (not just the progress bar)